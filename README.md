# maersk-ui-task
Shuffle and Sort Webpage using js for adding logic and simple HTML for creating webpage and CSS for styling UI.

## Specifications
- The page consists of 9 numbered cards which can be operated by using the shuffle and sort buttons
- Click the shuffle button to randomly rearrange the order of the cards
- Click the sort button to place the cards in ascending order (1 – 9)

## Tech Stacks
- Html
- CSS
- Vanila Javascript

## Setup or Usage
- git clone https://gitlab.com/mufeedpatel/maersk-ui-task.git
- Go inside maersk-ui-task folder
- Double click on index.html

## License
For open source projects, say how it is licensed.
